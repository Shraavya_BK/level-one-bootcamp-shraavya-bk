//WAP to find the sum of n fractions.


#include<stdio.h>

struct fraction
{
  int num;
  int denom;
};


int input_numerator()
{
  int x;
    printf("Enter numerator: ");
    scanf("%d",&x);
    return x;
}



int input_denominator()
{
  int y;
    printf("Enter denominator: ");
    scanf("%d",&y);
    return y;
}



int numr(int n1,int d1,int n2,int d2)
{
  int total;
  total = ((n1*d2)+(d1*n2));
  return total;
  
}

int denomr(int d1,int d2)
{
  return (d1*d2);
}

int gcd(int a,int b)
{
  int g=1;
  for(int i=1; i <= a && i <= b; i++)
  {
    if (a%i == 0 && b%i == 0)
    g = i;
  }
  return g;
}

int main()
{
  int n;
  printf("enter the number of elemets: ");
  scanf("%d",&n);
  struct fraction f1,f2,total;
  printf("enter the first fraction numerator: ");
  scanf("%d",&f1.num);
  printf("enter the first fraction denominator: ");
  scanf("%d",&f1.denom);
  for (int i=2;i<=n;i++)
  {
    f2.num = input_numerator();
    f2.denom = input_denominator();
    
    total.num = numr(f1.num,f1.denom,f2.num,f2.denom);
    total.denom = denomr(f1.denom,f2.denom);
    
    int g = gcd(total.num,total.denom);
    total.num = total.num/g;
    total.denom = total.denom/g;
    f1.num = total.num;
    f1.denom = total.denom;
  }

  printf("The sum of the fractions is: %d / %d\n",  total.num, total.denom);
  return 0;

}
