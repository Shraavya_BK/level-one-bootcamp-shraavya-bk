//Write a program to add two user input numbers using 4 functions.

int num1()
{
    int a;
    printf("Enter a number:\n");
    scanf("%d",&a);
    return a;
}
int num2()
{
    int b;
    printf("Enter a number:\n");
    scanf("%d",&b);
    return b;
}
int sum(int a,int b)
{
    int c;
    c = a + b;
    return c;
}
int output(int a,int b,int c)
{
    printf("The sum of the numbers %d and %d is %d\n",a,b,c);
}
int main()
{
    int p,q,r;
    p=num1();
    q=num2();
    r=sum(p,q);
    output(p,q,r);
    return 0;
}
