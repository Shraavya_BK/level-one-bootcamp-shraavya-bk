//WAP to find the volume of a tromboloid using 4 functions.

#include<stdio.h>
 
float input1()
{
  float p;
  printf("Enter the value of h :\n");
  scanf("%f",&p);
return p;
}
float input2()
{
  float q;
  printf("Enter the value of d :\n");
  scanf("%f",&q);
return q;
}
float input3()
{
  float r;
  printf("Enter the value of b :\n");
  scanf("%f",&r);
return r;
}
float compute(float p,float q,float r)
{
  float P1,P2,volume;
  P1 = p*q*r;
  P2 = q/r;
  volume = 1.0/3.0 * (P1 + P2);
return volume;
}
float output(float x)
{
 printf("The volume of tromboloid is %f\n",x);
}
int main()
{
  float h,d,b,Vol,OP;
  h = input1();
  d = input2();
  b = input3();
  Vol = compute(h,d,b);
  OP = output(Vol);
return 0;
}

