//WAP to find the distance between two point using 4 functions.

#include<stdio.h>
#include<math.h>
double input()
{
double x;
scanf("%lf",&x);
return x;
}
double compute(double w,double x,double y,double z)
{
double dist;
dist = sqrt(((y-w) *(y-w)) + ((z-x) *(z-x)));
return dist;
}
double output(double w,double x,double y,double z,double dist)
{
printf("The distance between %lf , %lf and %lf , %lf is %lf\n",w,x,y,z,dist);
}
int main()
{
double a1,b1,a2,b2,distance,OP;
printf("Enter the abscissa and ordinate of point 1:\n");
printf("Enter the abscissa and ordinate of point 2:\n");
a1 = input();
b1 = input();
a2 = input();
b2 = input();
distance = compute(a1,b1,a2,b2);
OP = output(a1,b1,a2,b2,distance);
return 0;
}


