//WAP to find the sum of two fractions.

#include<stdio.h>
#include<math.h>

struct frac
{
	int num,den;
};
typedef struct frac Frac ;

Frac input()

{
	Frac a;
	printf("Enter the numerator of fraction\n");
	scanf("%d",&a.num);

	printf("Enter the denominator of fraction\n");
	scanf("%d",&a.den);

	return a;
}
int GCD(int numerator,int denominator)
{
	
int i,gcd=1;
for(i=2;i<=numerator && i<=denominator;++i)
         	{
if(numerator%i==0 && denominator%i==0)
                       	{
gcd=i;
		}
 }
	return gcd;
}
Frac compute(Frac a,Frac b)

{
Frac s;
int numerator,denominator,gcd ,i;
numerator=(a.num*b.den)+(b.num*a.den);
denominator=(a.den*b.den);
gcd=GCD(numerator,denominator);
s.num=numerator/gcd;
s.dem=denominator/gcd;

return s;
}


void output(Frac a,Frac b,Frac s)
 	{
printf("the sum of two fractions %d / %d and %d / %d is %d / %d",a.num,a.den,b.num,b.den,s.num,s.den);
 	}

int main()
{
Fract p,q,s;

printf("fraction 1:\n");
p=input();

printf("fraction 2:\n");
q=input();

s=compute(num,den);
output(p,q,s);

return 0;
}

