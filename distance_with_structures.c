//WAP to find the distance between two points using structures and 4 functions.

#include<stdio.h>
#include <math.h>

struct points{
  int x;
  int y;
};

float dist(int a1,int b1,int a2,int b2)
{
  float z;
  z=sqrt(((a2-a1)*(a2-a1))+((b2-b1)*(b2-b1)));
  return z;
}

int input(){
  int a;
  scanf("%d",&a);
  return a;
}

void print(float ans){
  printf("the distance between 2 points is: %f",ans);
}

int main(){
  struct points first,second;
  printf("enter the first point: ");
  first.x= input();
  first.y = input();

  printf("enter the second point: ");
  second.x = input();
  second.y = input();

  float ans = dist(first.x,first.y,second.x,second.y);
  print(ans);
}
